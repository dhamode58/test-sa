package com.thedigitalgroup.beans;

public class DeliveryOptions {
    public String print;
    public boolean electronic;
    public boolean web;
	
    public String getPrint() {
		return print;
	}
	public void setPrint(String print) {
		this.print = print;
	}
	public boolean isElectronic() {
		return electronic;
	}
	public void setElectronic(boolean electronic) {
		this.electronic = electronic;
	}
	public boolean isWeb() {
		return web;
	}
	public void setWeb(boolean web) {
		this.web = web;
	}
    
    
    
    
}	
