package com.thedigitalgroup.beans;

public class Priority {
    
	public boolean mustMake;
    public boolean notLate;
    public boolean highProfile;
    public boolean training;
	
    public boolean isMustMake() {
		return mustMake;
	}
	public void setMustMake(boolean mustMake) {
		this.mustMake = mustMake;
	}
	public boolean isNotLate() {
		return notLate;
	}
	public void setNotLate(boolean notLate) {
		this.notLate = notLate;
	}
	public boolean isHighProfile() {
		return highProfile;
	}
	public void setHighProfile(boolean highProfile) {
		this.highProfile = highProfile;
	}
	public boolean isTraining() {
		return training;
	}
	public void setTraining(boolean training) {
		this.training = training;
	}
    
    
}
