package com.thedigitalgroup.beans;

public class Copies {
    
	public int bound;
    public int unbound;
    public int stapled;
    public int other;
    
	public int getBound() {
		return bound;
	}
	public void setBound(int bound) {
		this.bound = bound;
	}
	public int getUnbound() {
		return unbound;
	}
	public void setUnbound(int unbound) {
		this.unbound = unbound;
	}
	public int getStapled() {
		return stapled;
	}
	public void setStapled(int stapled) {
		this.stapled = stapled;
	}
	public int getOther() {
		return other;
	}
	public void setOther(int other) {
		this.other = other;
	}
    
    
}
