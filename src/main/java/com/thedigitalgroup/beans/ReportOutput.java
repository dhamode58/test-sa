package com.thedigitalgroup.beans;

public class ReportOutput {
 
        public String[] format;
        public String namePatern;
        public boolean zip;
        public boolean upload;
        public boolean gsEnglishOption;
        public int oneRecordPerPage;
        public String shortReportFormat;
        public boolean ownerIndex;
        public boolean federalClassifier;
        public boolean corsearchRef;
		public String[] getFormat() {
			return format;
		}
		public void setFormat(String[] format) {
			this.format = format;
		}
		public String getNamePatern() {
			return namePatern;
		}
		public void setNamePatern(String namePatern) {
			this.namePatern = namePatern;
		}
		public boolean isZip() {
			return zip;
		}
		public void setZip(boolean zip) {
			this.zip = zip;
		}
		public boolean isUpload() {
			return upload;
		}
		public void setUpload(boolean upload) {
			this.upload = upload;
		}
		public boolean isGsEnglishOption() {
			return gsEnglishOption;
		}
		public void setGsEnglishOption(boolean gsEnglishOption) {
			this.gsEnglishOption = gsEnglishOption;
		}
		public int getOneRecordPerPage() {
			return oneRecordPerPage;
		}
		public void setOneRecordPerPage(int oneRecordPerPage) {
			this.oneRecordPerPage = oneRecordPerPage;
		}
		public String getShortReportFormat() {
			return shortReportFormat;
		}
		public void setShortReportFormat(String shortReportFormat) {
			this.shortReportFormat = shortReportFormat;
		}
		public boolean isOwnerIndex() {
			return ownerIndex;
		}
		public void setOwnerIndex(boolean ownerIndex) {
			this.ownerIndex = ownerIndex;
		}
		public boolean isFederalClassifier() {
			return federalClassifier;
		}
		public void setFederalClassifier(boolean federalClassifier) {
			this.federalClassifier = federalClassifier;
		}
		public boolean isCorsearchRef() {
			return corsearchRef;
		}
		public void setCorsearchRef(boolean corsearchRef) {
			this.corsearchRef = corsearchRef;
		}
           
}
