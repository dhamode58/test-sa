package com.thedigitalgroup.beans;

public class DeleteOrder {
    public long id ;
    public OrderId[] searchOrders ;
    
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public OrderId[] getSearchOrders() {
		return searchOrders;
	}
	public void setSearchOrders(OrderId[] searchOrders) {
		this.searchOrders = searchOrders;
	}
    
    
}
