package com.thedigitalgroup.beans;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Error{
	private int errorCode;
	private String errorMsg;
	
	public Error() {
		// TODO Auto-generated constructor stub
	}

	public Error(int errorCode, String errorMsg) {
		super();
		this.errorCode = errorCode;
		this.errorMsg = errorMsg;
	}
	
}