package com.thedigitalgroup.beans;

public class AdditionalNotes {
	
    public String presearchClassing;
    public String searchSpecific;
    public String customerService;
    public String shipping;
    public String web;
    
	public String getPresearchClassing() {
		return presearchClassing;
	}
	public void setPresearchClassing(String presearchClassing) {
		this.presearchClassing = presearchClassing;
	}
	public String getSearchSpecific() {
		return searchSpecific;
	}
	public void setSearchSpecific(String searchSpecific) {
		this.searchSpecific = searchSpecific;
	}
	public String getCustomerService() {
		return customerService;
	}
	public void setCustomerService(String customerService) {
		this.customerService = customerService;
	}
	public String getShipping() {
		return shipping;
	}
	public void setShipping(String shipping) {
		this.shipping = shipping;
	}
	public String getWeb() {
		return web;
	}
	public void setWeb(String web) {
		this.web = web;
	}
    
    
}
