package com.thedigitalgroup.beans;

public class Notes {
    public String binding;
    public String special;
    
	public String getBinding() {
		return binding;
	}
	public void setBinding(String binding) {
		this.binding = binding;
	}
	public String getSpecial() {
		return special;
	}
	public void setSpecial(String special) {
		this.special = special;
	}
    
    
}
