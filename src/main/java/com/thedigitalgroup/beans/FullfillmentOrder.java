package com.thedigitalgroup.beans;

public class FullfillmentOrder {
    public IdName searchType;
    public String[] scopes;
    
	public IdName getSearchType() {
		return searchType;
	}
	public void setSearchType(IdName searchType) {
		this.searchType = searchType;
	}
	public String[] getScopes() {
		return scopes;
	}
	public void setScopes(String[] scopes) {
		this.scopes = scopes;
	}
    
    
}
