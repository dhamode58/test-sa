package com.thedigitalgroup.beans;

public class Recipient {
    public IdName contact;
    public Address shipping;
    public DeliveryOptions deliveryOptions;
    public Notes notes;
    public Copies copies;
    
	public IdName getContact() {
		return contact;
	}
	public void setContact(IdName contact) {
		this.contact = contact;
	}
	public Address getShipping() {
		return shipping;
	}
	public void setShipping(Address shipping) {
		this.shipping = shipping;
	}
	public DeliveryOptions getDeliveryOptions() {
		return deliveryOptions;
	}
	public void setDeliveryOptions(DeliveryOptions deliveryOptions) {
		this.deliveryOptions = deliveryOptions;
	}
	public Notes getNotes() {
		return notes;
	}
	public void setNotes(Notes notes) {
		this.notes = notes;
	}
	public Copies getCopies() {
		return copies;
	}
	public void setCopies(Copies copies) {
		this.copies = copies;
	}
    
    
}	
