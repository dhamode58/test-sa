package com.thedigitalgroup.beans;

public class AdditionalServices {
    
	public boolean markFamily;
    public boolean addWipo;
    public boolean webPages;
    public int socialMediaPages;
	
    public boolean isMarkFamily() {
		return markFamily;
	}
	public void setMarkFamily(boolean markFamily) {
		this.markFamily = markFamily;
	}
	public boolean isAddWipo() {
		return addWipo;
	}
	public void setAddWipo(boolean addWipo) {
		this.addWipo = addWipo;
	}
	public boolean isWebPages() {
		return webPages;
	}
	public void setWebPages(boolean webPages) {
		this.webPages = webPages;
	}
	public int getSocialMediaPages() {
		return socialMediaPages;
	}
	public void setSocialMediaPages(int socialMediaPages) {
		this.socialMediaPages = socialMediaPages;
	}
    
    
}
