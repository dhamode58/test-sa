package com.thedigitalgroup.beans;

public class Users {
    public String orderTakenBy ;
    public String updatedBy ;
	
    
    public String getOrderTakenBy() {
		return orderTakenBy;
	}
	public void setOrderTakenBy(String orderTakenBy) {
		this.orderTakenBy = orderTakenBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
    
    
}
