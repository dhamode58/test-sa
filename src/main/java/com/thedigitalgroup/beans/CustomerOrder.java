package com.thedigitalgroup.beans;

import java.util.Arrays;

public class CustomerOrder {

    public long id ;
    public String clientReference;
    public String status;
    public String product;
    public Customer customer;
    public IdName contact;
    public SearchOrder[] searchOrders;
	
    public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getClientReference() {
		return clientReference;
	}
	public void setClientReference(String clientReference) {
		this.clientReference = clientReference;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public IdName getContact() {
		return contact;
	}
	public void setContact(IdName contact) {
		this.contact = contact;
	}
	public SearchOrder[] getSearchOrders() {
		return searchOrders;
	}
	public void setSearchOrders(SearchOrder[] searchOrders) {
		this.searchOrders = searchOrders;
	}
	@Override
	public String toString() {
		return "CustomerOrder [id=" + id + ", clientReference=" + clientReference + ", status=" + status + ", product="
				+ product + ", customer=" + customer + ", contact=" + contact + ", searchOrders="
				+ Arrays.toString(searchOrders) + "]";
	}
    
   
}
