package com.thedigitalgroup.beans;

public class SearchOrder {
	
    public long id;
    public String[] mark;
    public String comments;
    public String gsText;
    public String secondaryGsText;
    public String icClasses;
    public String owner;
    public String dueDate;
    public String createTime;
    public String updateTime;
    public String approvedTime;
    public String uploadTime;
    public String reportCompletionDate;
    public String operationInTime;
    public String searchType;
    public String turnaround;
    public Design design;
    public AdditionalNotes additionalNotes;
    public AdditionalServices additionalServices;
    public String team;
    public Delivery delivery;   
    public FullfillmentOrder[] fulfillmentOrders;
    public Users users;
	
    
    public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String[] getMark() {
		return mark;
	}
	public void setMark(String[] mark) {
		this.mark = mark;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getGsText() {
		return gsText;
	}
	public void setGsText(String gsText) {
		this.gsText = gsText;
	}
	public String getSecondaryGsText() {
		return secondaryGsText;
	}
	public void setSecondaryGsText(String secondaryGsText) {
		this.secondaryGsText = secondaryGsText;
	}
	public String getIcClasses() {
		return icClasses;
	}
	public void setIcClasses(String icClasses) {
		this.icClasses = icClasses;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public String getApprovedTime() {
		return approvedTime;
	}
	public void setApprovedTime(String approvedTime) {
		this.approvedTime = approvedTime;
	}
	public String getUploadTime() {
		return uploadTime;
	}
	public void setUploadTime(String uploadTime) {
		this.uploadTime = uploadTime;
	}
	public String getReportCompletionDate() {
		return reportCompletionDate;
	}
	public void setReportCompletionDate(String reportCompletionDate) {
		this.reportCompletionDate = reportCompletionDate;
	}
	public String getOperationInTime() {
		return operationInTime;
	}
	public void setOperationInTime(String operationInTime) {
		this.operationInTime = operationInTime;
	}
	public String getSearchType() {
		return searchType;
	}
	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}
	public String getTurnaround() {
		return turnaround;
	}
	public void setTurnaround(String turnaround) {
		this.turnaround = turnaround;
	}
	public Design getDesign() {
		return design;
	}
	public void setDesign(Design design) {
		this.design = design;
	}
	public AdditionalNotes getAdditionalNotes() {
		return additionalNotes;
	}
	public void setAdditionalNotes(AdditionalNotes additionalNotes) {
		this.additionalNotes = additionalNotes;
	}
	public AdditionalServices getAdditionalServices() {
		return additionalServices;
	}
	public void setAdditionalServices(AdditionalServices additionalServices) {
		this.additionalServices = additionalServices;
	}
	public String getTeam() {
		return team;
	}
	public void setTeam(String team) {
		this.team = team;
	}
	public Delivery getDelivery() {
		return delivery;
	}
	public void setDelivery(Delivery delivery) {
		this.delivery = delivery;
	}
	public FullfillmentOrder[] getFulfillmentOrders() {
		return fulfillmentOrders;
	}
	public void setFulfillmentOrders(FullfillmentOrder[] fulfillmentOrders) {
		this.fulfillmentOrders = fulfillmentOrders;
	}
	public Users getUsers() {
		return users;
	}
	public void setUsers(Users users) {
		this.users = users;
	}
    
    
}
