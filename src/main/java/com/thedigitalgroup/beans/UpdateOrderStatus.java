package com.thedigitalgroup.beans;

public class UpdateOrderStatus {
    
	public long id ;
    public String status; 
    public String product ;
	
    
    public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
    
    
    
}
