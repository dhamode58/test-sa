package com.thedigitalgroup.beans;

public class Delivery {
    
	public Priority priority;
    public ReportOutput reportOutput;
    public Recipient[] recipients;
    public int totalCopies;
    
	public Priority getPriority() {
		return priority;
	}
	public void setPriority(Priority priority) {
		this.priority = priority;
	}
	public ReportOutput getReportOutput() {
		return reportOutput;
	}
	public void setReportOutput(ReportOutput reportOutput) {
		this.reportOutput = reportOutput;
	}

	public Recipient[] getRecipients() {
		return recipients;
	}
	public void setRecipients(Recipient[] recipients) {
		this.recipients = recipients;
	}
	public int getTotalCopies() {
		return totalCopies;
	}
	public void setTotalCopies(int totalCopies) {
		this.totalCopies = totalCopies;
	}
    
    
}
