package com.thedigitalgroup.restws;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import com.thedigitalgroup.beans.CustomerOrder;
import com.thedigitalgroup.beans.DeleteOrder;
import com.thedigitalgroup.beans.SearchOrder;
import com.thedigitalgroup.beans.TransferOrder;
import com.thedigitalgroup.beans.UpdateOrderStatus;
import com.thedigitalgroup.exception.DataFormatNotSupportedException;

@Path("mycontroller")
public class MyRestController {

	
	
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt() {
        return "Got it!";
    }

	
	
	@Path("/customerOrder")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveCustomerOrderInformation(CustomerOrder customerOrder) throws Exception{
		
		
			if(customerOrder.getId()<=0){
				throw new DataFormatNotSupportedException();
			}else{
				System.out.println(customerOrder.toString());

			}
		
		return Response.status(Status.ACCEPTED).entity("Recieved 'Customer Order' JSON with id: "+customerOrder.id).build();
		
	}
	
	
	@Path("/transferOrder")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response transferOrder(TransferOrder transferOrder) throws Exception{
		
		
			if(transferOrder.getId()<=0){
				throw new DataFormatNotSupportedException();
			}else{
				System.out.println(transferOrder.toString());

			}
		
		return Response.status(Status.ACCEPTED).entity("Recieved 'Transfer Order' JSON with id: "+transferOrder.id).build();
		
	}
	
	
	@Path("/changeOrderStatus")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response changeCustomerOrder(UpdateOrderStatus updateOrderStatus ) throws Exception{
		
		
			if(updateOrderStatus.getId()<=0){
				throw new DataFormatNotSupportedException();
			}else{
				System.out.println(updateOrderStatus.toString());
				
				//int i=1/0;

			}
		
		return Response.status(Status.ACCEPTED).entity("Recieved 'Change Order Status' JSON with id: "+updateOrderStatus.id).build();
		
	}
	
	@Path("/deleteOrder")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response deleteOrder(DeleteOrder deleteOrder ) throws Exception{
		

			if(deleteOrder.getId()<=0){
				throw new DataFormatNotSupportedException();
			}else{
				System.out.println(deleteOrder.toString());

			}
		
		return Response.status(Status.ACCEPTED).entity("Recieved 'Delete Order' JSON with id: "+deleteOrder.id).build();
		
	}
	
	
	@Path("/searchOrder")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response searchOrder(SearchOrder searchOrder ) throws Exception{

			if(searchOrder.getId()<=0){
				throw new DataFormatNotSupportedException();
			}else{
				System.out.println(searchOrder.toString());

			}

		return Response.status(Status.ACCEPTED).entity("Recieved 'Search Order/Order insert' JSON with id: "+searchOrder.id).build();
		
	}
}
