package com.thedigitalgroup.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class GlobalExceptionMapper implements ExceptionMapper<Throwable> {
	
	@Override
	public Response toResponse(Throwable arg0) {
		// TODO Auto-generated method stub
		return Response.status(500).entity(arg0.getMessage()).type("text/plain").build();
	}

}


