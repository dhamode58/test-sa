package com.thedigitalgroup.exception;

public class DataFormatNotSupportedException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8839472261456737336L;

	public DataFormatNotSupportedException() {
		super("Data Not Supported. Bad data in JSON.");
	}

}
