package com.thedigitalgroup.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class DataFormatNotSupportetMapper implements ExceptionMapper<DataFormatNotSupportedException> {

	@Override
	public Response toResponse(DataFormatNotSupportedException arg0) {

		return Response.status(Status.NOT_ACCEPTABLE).entity(arg0.getMessage()).type("text/plain").build();
	}

}
